/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartgulp',
  version: '3.0.0',
  description: 'lightweight gulp replacement'
}
